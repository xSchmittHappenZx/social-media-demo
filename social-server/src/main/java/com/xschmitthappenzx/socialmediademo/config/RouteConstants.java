package com.xschmitthappenzx.socialmediademo.config;

public class RouteConstants {

    /* Controller Routes */
    public final static String MAIN_ROUTE = "/";
    public final static String GET_GLOBAL_FEED_ROUTE = "globalFeed/";
    public final static String GET_USER_PROFILE_DATA = "profile/{id}";
    public final static String GET_USER_PROFILE_POST_DATA = "profile/{id}/posts";
}
