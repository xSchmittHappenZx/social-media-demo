package com.xschmitthappenzx.socialmediademo.controllers;

import com.xschmitthappenzx.socialmediademo.config.RouteConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = RouteConstants.MAIN_ROUTE)
public class MainViewController {

    @RequestMapping("")
    public ModelAndView getSocialMediaIndex(){
            return new ModelAndView("index");
    }
}
