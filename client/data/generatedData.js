/* this file is used during development in order to simulate data */
const data = {
  users: {
    user: {
      id: 1,
      username: "xODINx",
      email: "xODINx@email.com",
      profile_pic: ""
    },
    user: {
      id: 2,
      username: "xTHORx",
      email: "xTHORx@email.com",
      profile_pic: ""
    },
    user: {
      id: 3,
      username: "xTYRx",
      email: "xTYRx@email.com",
      profile_pic: ""
    },
    user: {
      id: 4,
      username: "xLOKIx",
      email: "xLOKIx@email.com",
      profile_pic: ""
    },
    user: {
      id: 5,
      username: "xFREYAx",
      email: "xFREYAx@email.com",
      profile_pic: ""
    }

  },
  posts: {
    post: {
      id: 1,
      content: "",
      media: ""
    },
    post: {
      id: 2,
      content: "",
      media: "",
      timestamp: ""
    },
    post: {
      id: 3,
      content: "",
      media: "",
      timestamp: ""
    },
    post: {
      id: 1,
      content: "",
      media: "",
      timestamp: ""
    },
    post: {
      id: 5,
      content: "",
      media: "",
      timestamp: ""
    },
    post: {
      id: 4,
      content: "",
      media: "",
      timestamp: ""
    },
    post: {
      id: 2,
      content: "",
      media: "",
      timestamp: ""
    },
    post: {
      id: 1,
      content: "",
      media: "",
      timestamp: ""
    },
  },
  friends: {
    /* Will come back to should there be time */
    /* Data would be a m to n relationship: id1 id2 */
  }
}
