import {
  SET_INIT_STATE
} from './initStateActionTypes'


import {
  loadData
} from './headerActions'

const setInitialState = () => (dispatch, getState) => {
  dispatch(loadData())
}

export {
  setInitialState
}
