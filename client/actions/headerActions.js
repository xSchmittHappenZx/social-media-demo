import {
  SET_USER_NAME,
  SET_USER_ID,
  IS_ERROR_FETCHING_USER_DATA
}  from './headerActionTypes'

import {
  getUsers,
  getUserName,
  getUserId
} from '../components/utility/utility'

const loadData = () => (dispatch, getState) => {

  dispatch(setUserName(getUserName()))
  dispatch(setUserId(getUserId()))
  /* Code to call endpoint for initial load */
}

const setUserName = value => {
  return {
    type: SET_USER_NAME,
    value
  }
}

const setUserId = value => {
  return {
    type: SET_USER_ID,
    value
  }
}

const setIsErrorFetchingUserDataSelector = value => {
  return {
    type: IS_ERROR_FETCHING_USER_DATA,
    value
  }
}

export {
  setUserName,
  setUserId,
  setIsErrorFetchingUserDataSelector,
  loadData
}
