# Social Media Demo - Client
Client was created using React/Redux and Webpack.

## Getting Started
Once you've pulled in the project, navigate to the client folder and enter the following command:
```
npm/yarn install
```
To run the client:
```
npm/yarn start
```
The client will now be running at
```
http://localhost:3000/
```
To build the client:
```
npm run/yarn build
```
