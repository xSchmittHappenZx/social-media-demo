import {combineReducers} from 'redux'
import {
  SET_USER_NAME,
  SET_USER_ID,
  IS_ERROR_FETCHING_USER_DATA
} from '../actions/headerActionTypes'

const username = (state = 'N/A', action) => {
  switch (action.type) {
    case SET_USER_NAME:
        return action.value
    default:
      return state
  }
}

const userId = (state = 'N/A', action) => {
  switch(action.type){
    case SET_USER_ID:
      return action.value
    default:
      return state
  }
}

const isErrorFetchingUserData = (state = false, action) => {
  switch (action.type) {
    case IS_ERROR_FETCHING_USER_DATA:
      return action.value
    default:
      return state
  }
}

const headerData = combineReducers({
  username,
  userId,
  isErrorFetchingUserData
})

export {
  headerData
}
