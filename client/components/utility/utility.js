import {
  data
} from '../../data/generatedData'

const getUsers= () => {
  if(process.env.NODE_ENV === 'production'){
    return document.getElementById("user-name").textContent
  }
  else{
    return data.users
  }
}

const getUserName = () => {
  // if(process.env.NODE_ENV === 'production'){
  //   return document.getElementById("userName").textContent
  // }
  // else{
    return "xODINx"
  // }
}

const getUserId = () => {
  // if(process.env.NODE_ENV === 'production'){
  //   return document.getElementById("userId").textContent
  // }
  // else{
    return "1"
  // }
}
export {
  getUsers,
  getUserName,
  getUserId
}
