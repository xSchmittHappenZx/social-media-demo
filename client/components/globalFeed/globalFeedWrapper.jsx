import {connect} from 'react-redux'

import GlobalFeed from './globalFeed'

const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const GlobalFeedWrapper = connect(
  mapStateToProps,
  mapDispatchToProps
)(GlobalFeed)

export default GlobalFeedWrapper
