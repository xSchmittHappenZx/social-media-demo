import {connect} from 'react-redux'

import Navbar from './navBar'

import {
  setUserName,
  setUserId,
  setIsErrorFetchingUserDataSelector,
} from '../../actions/headerActions'

const mapStateToProps = state => {
  return {
    username: state.headerData.username,
    userId: state.headerData.userId,
    isErrorFetchingUserData: setIsErrorFetchingUserDataSelector(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const NavBarWrapper = connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar)

export default NavBarWrapper
