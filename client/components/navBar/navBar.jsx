import React, {Component} from 'react'
import classNames from 'classnames'


class NavBar extends Component {

  render(){

    const {
      username,
      profileIcon,
      notifications
    } = this.props

    return(
      <div id="nav-bar">

        <div id="nav-bar-logo">

        </div>

        <div id="nav-bar-name">
          <h1>SOCIAL MEDIA DEMO</h1>
        </div>

        <div id="nav-bar-items">
          <div id="username" className="nav-bar-item">
            <h1>{username}</h1>
          </div>
          <div id="profile-icon" className="nav-bar-item">

          </div>
          <div id="notification-icon" className="nav-bar-item">

          </div>
          <div id="post-icon" className="nav-bar-item">

          </div>
        </div>
      </div>
    )
  }
}

export default NavBar
