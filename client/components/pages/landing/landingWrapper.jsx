import {connect} from 'react-redux'

import Landing from './landing'

const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

const LandingWrapper = connect(
  mapStateToProps,
  mapDispatchToProps
)(Landing)

export default LandingWrapper
