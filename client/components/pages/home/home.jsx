import React, {Component} from 'react'

import NavBarWrapper from '../../navBar/navBarWrapper'

class Home extends Component{

  render(){
      return(
        <div id="home">
          <NavBarWrapper/>
        </div>
      )
  }
}

export default Home
