import {connect} from 'react-redux'

import App from './app'

import {
  setInitialState
} from '../../actions/initStateActions'


const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {
    setInitialState: () => dispatch(setInitialState())
  }
}

const AppWrapper = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default AppWrapper
