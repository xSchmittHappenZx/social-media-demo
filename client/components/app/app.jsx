import React, {Component} from 'react'
import lodash from 'lodash'
import classNames from 'classnames'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import HomeWrapper from '../pages/home/homeWrapper'
import LandingWrapper from '../pages/landing/landingWrapper'

require('./app.scss')

class App extends Component{

  constructor(props){
    super(props)
  }

  componentDidMount(){
    this.props.setInitialState()

  }

  render(){
    return(
      <div id="app">
        <Router>
          <Route exact path="/" component={HomeWrapper}/>
        </Router>
      </div>
    )
  }
}

export default App
