# Social-Media-Demo: ROOT

- Just about a day logged so far: still need to implement a lot on the front-end, but it's a start.
- I have to pause to learn scala (Wanting to learn MPP languages), but hopefully will be able to get more finished soon!
- https://social-media-demo.cfapps.io : if you would like to see what's been deploy thus far
- Link to my other app: https://crypto-vader.cfapps.io

## Description:
- Vision Statement:
  - A fun way to demonstrate the tools in the tool kit.

- #### Setup:
  - pull the project from stash (public repo, no ssh key setup, or permissions needed to pull code)
  - have gradle installed locally for the next step
    - Min version: 4.3+, (4.2.1 & 4.2 have given me trouble in the past with node environments)
    - Gradle: After cloning the repo, cd into the directory and run in the terminal: gradle wrapper (windows machine & linux)
      - this will generate a gradle script putting a limit on our current gradle version, and if your local version doesn't match, it will pull from gradle the version set to the wrapper: 4.3+.

  - Can be ran in: Intellij for the server.
    - ${ENV}: environment variable that needs to be set to devl || prod
  - Front-end: can be ran external to the server with webpack-dev-server; a node library that has been added to this project

  - ##### To serve the front-end from the back end in an IDE:
    - Run gradlew clean build: this will build the front-end, downloading node in the process, bundle it, and copy it into the server's resource folder.
    - Import the gradle project into Intellij or Eclipse
    - Run the main class in the social-class package: this will start the spring boot app and serve the content at your machines localhost:8080/
